﻿using System;
using System.Collections.Generic;

namespace Craft_sample
{
    /*
     * Реализует функционал интвентаря 
     */ 
    public class Inventory
    {
        // список предметов в рюкзаке, отсортированный по id 
        public SortedList<int, SpecificItem> ListById;
        // список предметов в рюкзаке, отсортированных по типу слота
        public SortedList<SlotEnum, List<SpecificItem>> ListBySlot;

        public Inventory()
        {
            ListById = new SortedList<int, SpecificItem>();
            ListBySlot = new SortedList<SlotEnum, List<SpecificItem>>();
        }

        /*
         * Получить предмет из рюкзака по его id
         */
        public SpecificItem GetItem(int itemId)
        {
            SpecificItem res = ListById.ContainsKey(itemId) ? ListById[itemId] : null;  
            return res;
        }

        /*
         * Вернет тру, если такой предмет есть в рюкзаке и его достаточно
         */
        public bool IsEnoughItem(int itemId, int qty)
        {
            var item = ListById[itemId];
            return item != null && item.Quantity >= qty;
        }

        public void Clear()
        {
            ListById.Clear();
            ListBySlot.Clear();
        }

        public void Add(SpecificItem item)
        {
            AddToIdList(item);
            AddToSlotList(item);
        }        

        private void AddToIdList(SpecificItem item)
        {
            //Console.WriteLine($"AddToIdList itemId={itemId} qty={qty}");
            SpecificItem specificItem;
            if (ListById.ContainsKey(item.Item.Id)) 
            {
                specificItem = ListById[item.Item.Id];
                specificItem.Quantity++;
            }
            else
            {
                ListById.Add(item.Item.Id, item);
            }
        }
        
        private void AddToSlotList(SpecificItem specificItem)
        {
            List<SpecificItem> list;
            
            if (ListBySlot.ContainsKey(specificItem.Item.SlotType))
            {
                list = ListBySlot[specificItem.Item.SlotType];
                list.Add(specificItem);
            }
            else
            {
                list = new List<SpecificItem>();
                ListBySlot.Add(specificItem.Item.SlotType, list);
                list.Add(specificItem);
            }   
        }        
        
        /*
         * Уменьшить количество предметов в рюкзаке
         */
        public void Reduce(int itemId, int qty)
        {
            //Console.WriteLine($"Reduce itemId={itemId} qty={qty}");
            SpecificItem specificItem = ListById[itemId];
            if (specificItem.Quantity < qty) throw new Exception($"There is not enough Quantity for item {itemId}.");
            specificItem.Quantity -= qty;
        }

        /*
         * Удалить предмет из рюкзака
         */
        public void Remove(int itemId)
        {
            SpecificItem specificItem = ListById[itemId];
            RemoveFromIdList(itemId);
            RemoveFromSlotList(specificItem);
        }

        private SpecificItem RemoveFromIdList(int itemId)
        {
            var specificItem = ListById[itemId];
            if (specificItem == null) throw new Exception($"No item {itemId} in Inventory");
            ListById.Remove(itemId);
            return specificItem;
        }

        private void RemoveFromSlotList(SpecificItem specificItem)
        {
            var list = ListBySlot[specificItem.Item.SlotType];
            if (list == null) throw new Exception($"No item {specificItem.Item.Caption} in Inventory");
            list.Remove(specificItem);
        }
        
        public void ShowInventory()
        {
            Console.WriteLine("Инвентарь игрока:");
            foreach (var r in ListById)
            {
                Console.WriteLine(r.Value.ToString());
            }

            Console.WriteLine();
        }
    }
}