﻿using System.Text;

namespace Craft_sample
{
    /*
    *  Место размещения предмета
    */
    public class Slot 
    {
        // Используется для классификации слота по назначению
        public SlotEnum SlotType;
        // предмет, вставленный в слот
        public EquipItem Item;
        // кэш для возможного предмета
        public EquipItem PossibleItem;

        public Slot(SlotEnum slotType, EquipItem item = null)
        {
            SlotType = slotType;
            Item = item;
        }

        public override string ToString()
        {
            var builder = new StringBuilder($"{nameof(SlotType)}:{SlotType}, {nameof(Item)}:");
            builder.Append(Item != null ? Item.ToString() : "Empty").Append(" ").
                Append(nameof(PossibleItem)).Append(":").Append(PossibleItem != null ? PossibleItem.ToString() : "None");
            return builder.ToString();
        }
    }
}