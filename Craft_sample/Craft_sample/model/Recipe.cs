﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Craft_sample
{
    /*
     * Рецепт получения некого предмета
     */
    public class Recipe
    {
        private bool _canCraft;

        public int Id;
        // цена
        public float Price;
        // название рецепта
        public string Caption;
        // Что получим
        public EquipItem Target;
        // из чего делается
        public List<SpecificItem> Items;
        
        // список спуцифических предметов с расчитанным количеством. 
        // Показывает потребность в предметах для данного рецепта. Если CashedItems[n].Quantity < 0, то 
        // именно сколько прдметов CashedItems[n].Item не хватает для создания рецепта.
        // Если CashedItems == null, то это значит, что кэш не посчитан и его нужно создать GameFacade.CanCraft(recipe)
        // TODO После крафта надо сбрасывать кэш у всех рецептов, а не только у текущего 
        public List<SpecificItem> CashedItems;
        

        public bool CanCraft
        {
            get { return _canCraft && HasCache(); }
            set { _canCraft = value; }
        }

        public Recipe(int id, EquipItem target, SpecificItem[] components, string caption = "", float price = 0)
        {
            Init(id, target, components, caption, price);
        }

        protected void Init(int aId, EquipItem aTarget, SpecificItem[] components, string aCaption, float aPrice)
        {
            
            Items = new List<SpecificItem>();
            this.Id = aId;
            this.Target = aTarget;
            this.Caption = aCaption != "" ? aCaption : $"Рецепт для {Target.Caption}" ;
            this.Price = aPrice;
            
            foreach (var value in components)
            {
                Items.Add(value);
            }
        }
   
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(Target).Append(":").Append(System.Environment.NewLine);
            foreach (var item in Items)
            {
                builder.Append(" - ").Append(item.Item.Caption).Append(" ").Append(item.Item.Level).Append(" - ").
                    Append(item.Quantity).Append(" шт.");
            }
            return builder.ToString();   
        }

        public bool HasCache()
        {
            return CashedItems != null;
        }

        public void ClearCache()
        {
            CashedItems = null;
        }
    }
}