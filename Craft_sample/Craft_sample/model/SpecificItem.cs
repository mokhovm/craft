﻿using System.Text;

namespace Craft_sample
{
    /*
     *  Конкретный предмет с количеством 
     */
    public class SpecificItem
    {
        // предмет
        public EquipItem Item;
        // количество
        public int Quantity;

        public SpecificItem(EquipItem someItem, int qty)
        {
            Init(someItem, qty);
        }

        protected void Init(EquipItem part, int qty)
        {
            this.Item = part;
            this.Quantity = qty;
        }
        
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(Item.Caption).Append(" ").Append(Item.Level).Append(" for ").Append(Item.SlotType).
                Append(" - ").Append(Quantity).Append(" шт.");
            return builder.ToString();   
        }        
        
    }
}