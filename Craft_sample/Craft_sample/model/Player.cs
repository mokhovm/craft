﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Craft_sample
{
    /*
     *  пользователь игры
     */
    public class Player
    {   
        // Рюкзак с предметами
        public Inventory Inventory;
        // список доступных героев
        public SortedList<int, Hero> Heroes;

        public Player()
        {
            Inventory = new Inventory ();
            Heroes = new SortedList<int, Hero>();            
        }

        /*
         * Возращает первого героя. Используется для теста 
         */
        public Hero GetAvailableHero()
        {
            return Heroes.Count > 0 ? Heroes[Heroes.Keys[0]] : null; 
        }

    }
}