﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Craft_sample
{

    /*
     *  герой игры
     */
    public class Hero
    {

        public int Id;
        // имя героя
        public string Name { get; set; }

        // слоты для установки предметов
        public readonly SortedList<SlotEnum, Slot> Slots;

        public Hero(int id, string name)
        {
            this.Id = id;
            this.Name = name;
            Slots = new SortedList<SlotEnum, Slot>
            {
                {SlotEnum.Chest, new Slot(SlotEnum.Chest)},
                {SlotEnum.Hand, new Slot(SlotEnum.Hand)},
                {SlotEnum.Head, new Slot(SlotEnum.Head)},
                {SlotEnum.Leg, new Slot(SlotEnum.Leg)},
                {SlotEnum.Neck, new Slot(SlotEnum.Neck)},
                {SlotEnum.Weapon, new Slot(SlotEnum.Weapon)}
            };
        }

        /*
         * Стирает кэш возможных предметов
         */
        public void ClearPossibleItems()
        {
            foreach (var slot in Slots)
            {
                slot.Value.PossibleItem = null;
            }
        }

        /*
         * Можно ли поставить какой-то предмет в указанный слот
         */
        public bool CanWearSlot(SlotEnum slot, SortedList<int, EquipItem> someInventory)
        {
            return Slots[slot].PossibleItem != null;            
        }
        

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(nameof(Name)).Append(":").Append(Name).Append(System.Environment.NewLine);
            foreach (var value in Slots.Values)
            {
                builder.Append(value).Append(System.Environment.NewLine);    
            }
            return builder.ToString();
        }
    }
}