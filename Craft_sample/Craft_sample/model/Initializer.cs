﻿using System.Diagnostics;

namespace Craft_sample
{
    /*
     * Умеет инициализировать тестовые данные для фасада
     */
    public class Initializer 
    {
        private GameFacade _facade;

        public Initializer(GameFacade facade)
        {
            _facade = facade;
        }

        public void CreateItems()
        {
            // инициализируем справочник предметов
            AddItemToDict(new EquipItem(id:1, "Amulet", level:1, SlotEnum.Neck, ItemTypeEnum.Whole));
            AddItemToDict(new EquipItem(id:2, "Ring", level:1, SlotEnum.Hand, ItemTypeEnum.Whole));
            AddItemToDict(new EquipItem(id:3, "Amulet Part", level:2, SlotEnum.Neck, ItemTypeEnum.Part));
            AddItemToDict(new EquipItem(id:4, "Amulet", level:2, SlotEnum.Neck, ItemTypeEnum.Whole));
            AddItemToDict(new EquipItem(id:5, "Sword", level:1, SlotEnum.Weapon, ItemTypeEnum.Whole));
            AddItemToDict(new EquipItem(id:6, "Sword", level:2, SlotEnum.Weapon, ItemTypeEnum.Whole));
            AddItemToDict(new EquipItem(id:7, "MagicBook", level:1, SlotEnum.Hand, ItemTypeEnum.Whole));
            AddItemToDict(new EquipItem(id:8, "MagicBook Fragment", level:2, SlotEnum.Hand, ItemTypeEnum.Fragment));
            AddItemToDict(new EquipItem(id:9, "MagicBook Part", level:2, SlotEnum.Hand, ItemTypeEnum.Part));
            AddItemToDict(new EquipItem(id:10, "MagicBook", level:2, SlotEnum.Hand, ItemTypeEnum.Whole));
        }
        
        public void CreatePlayer()
        {
            // Добавим тестовые данные: героев и кое-что в рюкзак
            AddHero(new Hero(id:1, "Анатолий Вассерман"));
            AddToInventory(itemId: 1, qty: 5);
            AddToInventory(itemId: 2, qty: 3);
            AddToInventory(itemId: 3, qty: 19);
            AddToInventory(itemId: 5, qty: 1);
        }

        public void AddToInventory(int itemId, int qty)
        {
            _facade.Player.Inventory.Add(new SpecificItem(GetItemById(itemId), qty));
        }

        public void AddItemToDict(EquipItem item)
        {
            _facade.EquipItemsDict.Add(item.Id, item);
        }
        
        public void RemoveItemFromDict(EquipItem item)
        {
            _facade.EquipItemsDict.Remove(item.Id);
        }

        public void CreateReceipes()
        {
            // создадим несколько рецептов
            AddRecipe(new Recipe(id:4, GetItemById(4), 
                new SpecificItem[] {new SpecificItem(GetItemById(2), qty:2), 
                                    new SpecificItem(GetItemById(3), qty:5)}));
            AddRecipe(new Recipe(id:9, GetItemById(9), 
                new SpecificItem[] {new SpecificItem(GetItemById(8), qty:10)}));
            AddRecipe(new Recipe(id:10, GetItemById(10), 
                new SpecificItem[] {new SpecificItem(GetItemById(7), qty:2), 
                                    new SpecificItem(GetItemById(9), qty:10)}));
        }

        /*
         * Верет по id предмета сам предмет
         */
        public EquipItem GetItemById(int id)
        {
            Debug.Assert(_facade.EquipItemsDict.Count > 0, "Item dictionary is not initialized");
            return _facade.EquipItemsDict[id];
        }

        public void AddRecipe(Recipe recipe)
        {
            _facade.Recipes.Add(recipe.Id, recipe);
        }

        public void AddHero(Hero hero)
        {
            _facade.Player.Heroes.Add(hero.Id, hero);
        }

        public void Run()
        {
            CreateItems();
            CreateReceipes();
            CreatePlayer();

        }
    }
}