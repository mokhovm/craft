﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Craft_sample
{
    /*
     * Фасад приложения
     */
    public class GameFacade
    {
        private static volatile GameFacade _instance;
        protected static readonly object _syncObj = new object();

        public static GameFacade Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncObj)
                    {
                        if (_instance == null)
                        {
                            _instance = new GameFacade();
                        }
                    }
                }

                return _instance;
            }
        }

        /*
         * Пользователь игры
         */
        public Player Player;

        /*
         *  Список рецептов
         */
        public SortedList<int, Recipe> Recipes;

        /*
         * Список всех игровых предметоа
         */
        public SortedList<int, EquipItem> EquipItemsDict;

        public Initializer Initializer;

        public GameFacade()
        {
            _instance = this;
            Initializer  = new Initializer(this);
            Player = new Player();
            Recipes = new SortedList<int, Recipe>();
            EquipItemsDict = new SortedList<int, EquipItem>();
        }
    
        public void ShowRecipes()
        {
            Console.WriteLine("Справочник рецептов:");
            foreach (var r in Recipes.Keys)
            {
                Console.WriteLine(Recipes[r].ToString());
            }
            Console.WriteLine("");
        }

        public void ShowItems()
        {
            Console.WriteLine("Справочник предметов:");
            foreach (var r in EquipItemsDict.Keys)
            {
                Console.WriteLine(EquipItemsDict[r].ToString());
            }
            Console.WriteLine("");
        }
        
        public void ShowHeroes()
        {
            Console.WriteLine("Герои игрока:");
            foreach (var key in Player.Heroes.Keys)
            {
                Console.WriteLine(Player.Heroes[key].ToString());
            }
            //Console.WriteLine("");
        }
        
        /*
         *  Создать предмет. 
         */
        public EquipItem CraftItem(int itemId)
        {
            EquipItem res = null;
            var recipe = GameFacade.Instance.Recipes[itemId];
            if (recipe != null)
            {
                if (CanCraft(recipe))
                {
                    foreach (var specificItem in recipe.CashedItems)
                    {
                        var item = Player.Inventory.GetItem(specificItem.Item.Id);
                        if (item != null && specificItem.Quantity >= 0)
                        {
                            item.Quantity = specificItem.Quantity;
                        }
                        else 
                            throw new Exception(string.Format($"Unable to create a new item {specificItem.Item}"));
                    }

                    Player.Inventory.Add(new SpecificItem(recipe.Target, 1));
                    recipe.ClearCache();
                    res = recipe.Target;
                }
            }
            return res;
        }
        
        /*
        * Инициализирует availableItems для определения того, есть ли необходимые ресурсы для крафта рецепта
        * в неком инвентаре someInventory
        */
        public bool CanCraft(Recipe recipe)
        {
            bool res = false;
            if (!recipe.HasCache())
            {
                int availableQty = 0;
                recipe.CashedItems = new List<SpecificItem>();
                //recipe.CashedTarget = recipe.Target;
                
                foreach (var recipeItem in recipe.Items)
                {
                    var delta = 0;
                    var inventoryItem = Player.Inventory.GetItem(recipeItem.Item.Id);
                    if (inventoryItem != null) delta = inventoryItem.Quantity;
                    delta -= recipeItem.Quantity;  
                    recipe.CashedItems.Add(new SpecificItem(recipeItem.Item, delta));
                    if (delta >= 0) availableQty++;
                }
                recipe.CanCraft = availableQty == recipe.Items.Count;
            }
            res = recipe.CanCraft;

            return res;
        }      
        
        /*
         * Автоматически надевает подходящие вещи на игрока 
         */
        public void AutoWear(Hero hero)
        {
            UpdatePossibleItem(hero);
            foreach (var slot in hero.Slots.Values)
            {
                if (slot.PossibleItem != null)
                {
                    slot.Item = slot.PossibleItem;
                    slot.PossibleItem = null;
                    Player.Inventory.Reduce(slot.Item.Id, 1);
                }
            }
            
        }
        
        /*
         *  Заполнит кэш возможных предметов для одевания персонажа
         */
        public void UpdatePossibleItem(Hero hero)
        {
            foreach (var value in Player.Inventory.ListById.Values)
            {
                if (value.Item != null && value.Item.ItemType == ItemTypeEnum.Whole && value.Quantity > 0) 
                {
                    if (hero.Slots.ContainsKey(value.Item.SlotType) && hero.Slots[value.Item.SlotType].Item == null) 
                        hero.Slots[value.Item.SlotType].PossibleItem = value.Item;
                }
            }
        }        

    }
}    
    
