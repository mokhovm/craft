﻿using System;
using System.Text;

namespace Craft_sample
{
    /*
     * Типы слотов
     */
    public enum SlotEnum
    {
        Head,
        Neck,
        Chest,
        Hand,
        Leg, 
        Weapon
    }
    
    /*
     * Типы предметов экипировки
     */
    public enum ItemTypeEnum
    {
        Whole,
        Part,
        Fragment,
    }
    
    /*
     * Некий базовый игровой предмет
     */
    public class EquipItem : IEquatable<EquipItem>
    {
        // уникальный идентификатор предмета
        public int Id { get; set; }
        // Название предмета
        public string Caption { get; set; }
        // описание
        public string Description { get; set;}
        // уровень
        public int Level { get; set; }
        // тип слота, в который вставляется предмет
        public SlotEnum SlotType { get; set; }
        // тип предмета
        public ItemTypeEnum ItemType { get; set; }

        public EquipItem(int id, string caption, int level, SlotEnum slotType, ItemTypeEnum itemType)
        {
            this.Id = id; 
            this.Caption = caption;
            this.Level = level;
            this.SlotType = slotType;
            this.ItemType = itemType;
        }


        public bool Equals(EquipItem other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id && Caption == other.Caption && Description == other.Description && 
                Level == other.Level && SlotType == other.SlotType && ItemType == other.ItemType;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((EquipItem) obj);
        }

        public override int GetHashCode()
        {
            const int HASH_CODE_MULTIPLIER = 397;
            unchecked
            {
                var hashCode = Id;
                hashCode = (hashCode * HASH_CODE_MULTIPLIER) ^ (Caption != null ? Caption.GetHashCode() : 0);
                hashCode = (hashCode * HASH_CODE_MULTIPLIER) ^ (Description != null ? Description.GetHashCode() : 0);
                hashCode = (hashCode * HASH_CODE_MULTIPLIER) ^ Level;
                hashCode = (hashCode * HASH_CODE_MULTIPLIER) ^ (int) SlotType;
                hashCode = (hashCode * HASH_CODE_MULTIPLIER) ^ (int) ItemType;
                return hashCode;
            }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(Caption).Append(" ").Append(Level).Append(" for ").Append(SlotType);
            return builder.ToString();
        }
    }

    
}