﻿using System;

namespace Craft_sample
{
    internal class Program
    {
        /*
        Дано.
        1) Список предметов, в которые могут  быть экипированы персонажи (equip item). У каждого предмета есть уровень.
            Некоторые предметы можно создавать по рецепту из частей (данного и меньшего уровня), и предметов меньшего уровня.
            Части могут быть также созданы, но уже из фрагментов. У рецепта есть стоимость в игровой валюте.
        2) Инвентарь
        3) Персонажи, которым сейчас нужны определенный набор предметов
 
        Надо.
            Написать минимальную систему крафта, которая для предметов, нужных для персонажей игрока, позволяет:
        a.1) Быстро ответить, достаточно ли данного предмета (частей \ фрагментов) - Inventory.IsEnoughItem()
        a.2) Быстро ответить, можно ли скрафтить данный предмет (для создаваемых, которых недостаточно) - GameFacade.CanCraft()
        б) Скрафтить данный предмет GameFacade.Craft()
            Пример списка предметов:
        - Amulet 1,
        - Ring 1
        - Amulet Part 2
        - Amulet 2 (создаётся из  Ring 1 (x 2),  Amulet Part 2 (x 5)),
        - Sword 1
        - Sword 2
        - Magicbook 1
        - Magicbook 2 Fragment
        - Magicbook 2 Part ( создаётся из  Magicbook 2 Fragment (x 10))
            - Magicbook 2 ( Magicbook 1 (x 2),  Magicbook 2 Part ( x 10))

        */
        
        public static void Main(string[] args)
        {
            // ид предмета для проверки
            const int ITEM1_ID = 1;
            const int ITEM1_QTY = 2;
            // ид предмета для создания
            const int ITEM2_ID = 4;
            // ид рецепта для создения предмета ITEM_ID2 
            const int RECIPE_ID = 4; 
            
            GameFacade facade = new GameFacade();
            facade.Initializer.Run();

            var someHero = facade.Player.GetAvailableHero();
            facade.UpdatePossibleItem(someHero);

            facade.ShowItems();
            facade.ShowRecipes();
            facade.ShowHeroes();
            facade.Player.Inventory.ShowInventory();

            var item = facade.EquipItemsDict[ITEM1_ID];
            var qty = ITEM1_QTY;
            Console.WriteLine("Вопросы:");
            Console.WriteLine($"Вопрос a.1. Есть ли у игрока {item} {qty} шт?");
            Console.WriteLine($"{facade.Player.Inventory.IsEnoughItem(item.Id, qty)}");
            Console.WriteLine();

            var recipe = facade.Recipes[RECIPE_ID]; 
            Console.WriteLine($"Вопрос a.2. А можно ли создать {recipe}?");
            Console.WriteLine($"{facade.CanCraft(recipe)}");
            Console.WriteLine();
            
            Console.WriteLine($"Вопрос б. Пробуем создать {recipe.Target}: ");
            var newItem = facade.CraftItem(ITEM2_ID);
            Console.WriteLine($"Чпок!");
            if (newItem != null)
                Console.WriteLine($"Теперь у тебя есть {newItem}! ");
            else 
                Console.WriteLine($"Что-то пошло не так...");
            Console.WriteLine();

            facade.Player.Inventory.ShowInventory();
            Console.WriteLine($"Приоденем героя в новый прикид...");
            facade.AutoWear(someHero);
            facade.ShowHeroes();
            facade.Player.Inventory.ShowInventory();
            
            Console.WriteLine("Program complete");
            //Console.ReadKey();
        }
    }
}