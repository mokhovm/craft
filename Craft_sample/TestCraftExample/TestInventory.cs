﻿using System;
using Craft_sample;
using NUnit.Framework;
using TestCraftExample.Properties;

namespace TestCraftExample
{
    [TestFixture]
    public class TestInventory
    {
        private GameFacade _facade;
        
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Console.WriteLine("OneTimeSetUp");
            _facade = new GameFacade();
            foreach (var item in TestDataEmitter.SampleItemList)
            {
                _facade.Initializer.AddItemToDict(item);    
            }
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Console.WriteLine("OneTimeTearDown");
            Console.WriteLine(_facade.EquipItemsDict.Count);
            _facade = null;
        }
        
        [SetUp]
        public void SetUp()
        {
            Console.WriteLine("Setup");
            Console.WriteLine( _facade.Player.Inventory.ListById.Count);
        }

        [TearDown]
        public void TearDown()
        {
            Console.WriteLine("TearDown");
            Console.WriteLine( _facade.Player.Inventory.ListById.Count);
        }

     
        [Test, Order(1), TestCaseSource(typeof(TestDataEmitter), nameof(TestDataEmitter.SampleIdList)),
         Description("Добавляем пердметы в инвентарь игрока")]
        public void Test_AddItemToInventory(int itemId, int qty)
        {
            _facade.Initializer.AddToInventory(itemId, qty);
            SpecificItem specificItem = _facade.Player.Inventory.GetItem(itemId);
            Assert.AreEqual(specificItem.Quantity, qty);
            Assert.AreEqual(specificItem.Item.Id, itemId );
            // убедимся, что и во втором списке появился данный предмет
            var specificItem2 = _facade.Player.Inventory.ListBySlot[specificItem.Item.SlotType].
                Find(item => item.Item.Id == itemId);
            Assert.AreEqual(specificItem.Item.Id, specificItem2.Item.Id);
            
        }

        [Test, Order(2), TestCaseSource(typeof(TestDataEmitter), nameof(TestDataEmitter.SampleIdList)),
         Description("Уменьшаем количество предметов на один")]
        public void Test_ReduceItemInInventory(int itemId, int qty)
        {
            _facade.Player.Inventory.Reduce(itemId, 1);
            SpecificItem specificItem = _facade.Player.Inventory.GetItem(itemId);
            Assert.AreEqual(qty - 1, specificItem.Quantity);
            Assert.AreEqual(specificItem.Item.Id, itemId );
        }

        [Test, Order(3), TestCaseSource(typeof(TestDataEmitter), nameof(TestDataEmitter.SampleIdList)),
         Description("Очищаем инвентарь игрока")]
        public void Test_RemoveItemFromInventory(int itemId, int qty)
        {
            SpecificItem specificItem1 = _facade.Player.Inventory.GetItem(itemId);
            SlotEnum slot = specificItem1.Item.SlotType;
            _facade.Player.Inventory.Remove(itemId);
            SpecificItem specificItem2 = _facade.Player.Inventory.GetItem(itemId);
            Assert.IsNull(specificItem2);
            // убедимся, что и во втором списке  данный предмет пропал
            specificItem2 = _facade.Player.Inventory.ListBySlot[slot].Find(item => item.Item.Id == itemId);
            Assert.IsNull(specificItem2);
        }
        
        
    }
}