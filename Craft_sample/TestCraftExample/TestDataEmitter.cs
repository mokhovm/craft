﻿using Craft_sample;

namespace TestCraftExample.Properties
{
    /*
    *  Просто источник тестовых данных
    */
    public class TestDataEmitter
    { 
        public static readonly EquipItem[] SampleItemList =
        {
            new EquipItem(1, "Amulet", 1, SlotEnum.Neck, ItemTypeEnum.Whole),            
            new EquipItem(2, "Ring", 2, SlotEnum.Hand, ItemTypeEnum.Whole),
            new EquipItem(3, "Amulet Part", 3, SlotEnum.Neck, ItemTypeEnum.Part),
            new EquipItem(4, "Amulet", 2, SlotEnum.Neck, ItemTypeEnum.Whole),
            new EquipItem(5, "Sword", 1, SlotEnum.Weapon, ItemTypeEnum.Whole),
            new EquipItem(6, "Sword", 2, SlotEnum.Weapon, ItemTypeEnum.Whole),
            new EquipItem(7, "MagicBook", 1, SlotEnum.Hand, ItemTypeEnum.Whole),
            new EquipItem(8, "MagicBook Fragment", 2, SlotEnum.Hand, ItemTypeEnum.Fragment),
            new EquipItem(9, "MagicBook Part", 2, SlotEnum.Hand, ItemTypeEnum.Part),
            new EquipItem(10, "MagicBook", 2, SlotEnum.Hand, ItemTypeEnum.Whole)           
        };
        
        public static readonly object[] SampleIdList =
        {
            new object[] {1, 5},
            new object[] {2, 3},
            new object[] {3, 7},
            new object[] {4, 5},
            new object[] {5, 4},
            new object[] {6, 3},
            new object[] {7, 2},
            new object[] {8, 4},
            new object[] {9, 5},
            new object[] {10, 6}
        };       
        
        public static readonly object[] SampleEquipItem =
        {
            //id, caption, level, slotType, itemType
            new object[] {1, "Amulet", 1, SlotEnum.Neck, ItemTypeEnum.Whole},
            new object[] {2, "Ring", 2, SlotEnum.Hand, ItemTypeEnum.Whole},
            new object[] {3, "Amulet Part", 3, SlotEnum.Neck, ItemTypeEnum.Part},
            new object[] {4, "Amulet", 2, SlotEnum.Neck, ItemTypeEnum.Whole},
            new object[] {5, "Sword", 1, SlotEnum.Weapon, ItemTypeEnum.Whole},
            new object[] {6, "Sword", 2, SlotEnum.Weapon, ItemTypeEnum.Whole},
            new object[] {7, "MagicBook", 1, SlotEnum.Hand, ItemTypeEnum.Whole},
            new object[] {8, "MagicBook Fragment", 2, SlotEnum.Hand, ItemTypeEnum.Fragment},
            new object[] {9, "MagicBook Part", 2, SlotEnum.Hand, ItemTypeEnum.Part},
            new object[] {10, "MagicBook", 2, SlotEnum.Hand, ItemTypeEnum.Whole}
        };      
        
           
    }
}