﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Craft_sample;
using NUnit.Framework;
using TestCraftExample.Properties;

namespace TestCraftExample
{
    [TestFixture]
    public class TestItemDictionary
    {
        private GameFacade _facade;
        
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Console.WriteLine("OneTimeSetUp");
            _facade = new GameFacade();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Console.WriteLine("OneTimeTearDown");
            Console.WriteLine(_facade.EquipItemsDict.Count);
            _facade = null;
        }
        
        [SetUp]
        public void SetUp()
        {
            Console.WriteLine("Setup");
        }

        [TearDown]
        public void TearDown()
        {
            Console.WriteLine("TearDown");
            
        }        
        
        [Test, Order(1)]
        [TestCaseSource(typeof(TestDataEmitter), "SampleItemList")]
        public void Test_AddItemToDictionary(EquipItem item)
        {
            Console.WriteLine("Check item {0}", item.Caption);
            _facade.Initializer.AddItemToDict(item);
            Assert.True(_facade.EquipItemsDict.ContainsKey(item.Id));
        }

        [Test, Order(1000)]
        [TestCaseSource(typeof(TestDataEmitter), "SampleItemList")]
        public void Test_RemoveItemFromDictionary(EquipItem item)
        {
            Console.WriteLine("Check item {0}", item.Caption);
            _facade.Initializer.RemoveItemFromDict(item);
            Assert.False(_facade.EquipItemsDict.ContainsKey(item.Id));
        }


    }
    

}