﻿using System;
using Craft_sample;
using NUnit.Framework;
using TestCraftExample.Properties;

namespace TestCraftExample
{
    [TestFixture]
    public class TestEquipItem
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Console.WriteLine("OneTimeSetUp");
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Console.WriteLine("OneTimeTearDown");
        }
        
        [SetUp]
        public void SetUp()
        {
            Console.WriteLine("Setup");
        }

        [TearDown]
        public void TearDown()
        {
            Console.WriteLine("TearDown");
            
        }    
        
        [Test]
        [TestCaseSource(typeof(TestDataEmitter), nameof(TestDataEmitter.SampleEquipItem))]
        public void Test_Equal(int id, string caption, int level, SlotEnum slotType, ItemTypeEnum itemType)
        {
            var item1 = new EquipItem(id, caption, level, slotType, itemType);
            var item2 = new EquipItem(id, caption, level, slotType, itemType);
            var item3 = new EquipItem(id, caption + "x", level, slotType, itemType);
            var item4 = new EquipItem(id, caption, level + 1, slotType, itemType);
            Assert.IsTrue(item1.Equals(item2));
            Assert.IsFalse(item1.Equals(item3));
            Assert.IsFalse(item1.Equals(item4));
            Assert.IsFalse(item1.Equals(null));
        }
    }
}