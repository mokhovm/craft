﻿using System;
using Craft_sample;
using NUnit.Framework;
using TestCraftExample.Properties;

namespace TestCraftExample
{
    [TestFixture]
    public class TestFacade
    {
        private GameFacade _facade;
        
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Console.WriteLine("OneTimeSetUp");
            _facade = new GameFacade();
            foreach (var item in TestDataEmitter.SampleItemList)
            {
                _facade.Initializer.AddItemToDict(item);    
            }
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Console.WriteLine("OneTimeTearDown");
            Console.WriteLine(_facade.EquipItemsDict.Count);
            _facade = null;
        }
        
        [SetUp]
        public void SetUp()
        {
            Console.WriteLine("Setup");
            _facade.Initializer.AddToInventory(itemId: 1, qty: 5);
            _facade.Initializer.AddToInventory(itemId: 2, qty: 3);
            _facade.Initializer.AddToInventory(itemId: 3, qty: 19);
            _facade.Initializer.AddToInventory(itemId: 5, qty: 1);
            
            _facade.Initializer.AddRecipe(new Recipe(id:4, target:_facade.Initializer.GetItemById(4), 
                new SpecificItem[] {new SpecificItem(_facade.Initializer.GetItemById(2), qty:2), 
                    new SpecificItem(_facade.Initializer.GetItemById(3), qty:5)}));
            _facade.Initializer.AddRecipe(new Recipe(id:9, target:_facade.Initializer.GetItemById(9), 
                new SpecificItem[] {new SpecificItem(_facade.Initializer.GetItemById(8), qty:10)}));
            _facade.Initializer.AddRecipe(new Recipe(id:10, target:_facade.Initializer.GetItemById(10), 
                new SpecificItem[] {new SpecificItem(_facade.Initializer.GetItemById(7), qty:2), 
                    new SpecificItem(_facade.Initializer.GetItemById(9), qty:10)}));
        }

        [TearDown]
        public void TearDown()
        {
            Console.WriteLine("TearDown");
            _facade.Recipes.Clear();
            _facade.Player.Inventory.Clear();
        }          
        
        [Test]
        public void Test_CanCraft()
        {
            var recipe = _facade.Recipes[4];
            Assert.AreEqual(_facade.CanCraft(recipe), true);
            recipe = _facade.Recipes[9];
            Assert.AreEqual(_facade.CanCraft(recipe), false);
            recipe = _facade.Recipes[10];
            Assert.AreEqual(_facade.CanCraft(recipe), false);
        }
        
        [Test]
        public void Test_CraftItem()
        {
            var recipe = _facade.Recipes[4];
            _facade.CraftItem(recipe.Target.Id);
            // проверим, получили ли предмет
            SpecificItem specificItem =_facade.Player.Inventory.ListById[4];
            Assert.IsNotNull(specificItem);
            Assert.AreEqual(specificItem.Item.Id, 4);
            Assert.AreEqual(specificItem.Quantity, 1);
            // проверим, уменьшились ли ресурсы
            specificItem = _facade.Player.Inventory.ListById[2];
            Assert.AreEqual(specificItem.Item.Id, 2);
            Assert.AreEqual(specificItem.Quantity, 1);
            specificItem = _facade.Player.Inventory.ListById[3];
            Assert.AreEqual(specificItem.Item.Id, 3);
            Assert.AreEqual(specificItem.Quantity, 14);
            
            // девятый рецепт сделать нельзя
            recipe = _facade.Recipes[9];
            var item = _facade.CraftItem(recipe.Target.Id);
            Assert.IsNull(item);
            
            // десятый рецепт сделать нельзя
            recipe = _facade.Recipes[10];
            item = _facade.CraftItem(recipe.Target.Id);
            Assert.IsNull(item);            

        }

        [Test]
        public void Test_EnoughItem()
        {
            Assert.IsTrue(_facade.Player.Inventory.IsEnoughItem(1, 5));
            Assert.IsFalse(_facade.Player.Inventory.IsEnoughItem(1, 10));
            Assert.IsTrue(_facade.Player.Inventory.IsEnoughItem(2, 3));
            Assert.IsFalse(_facade.Player.Inventory.IsEnoughItem(2, 20));
        }

    }
}