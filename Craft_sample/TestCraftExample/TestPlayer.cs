﻿using System;
using Craft_sample;
using NUnit.Framework;

namespace TestCraftExample
{
    [TestFixture]
    public class TestPlayer
    {
        private GameFacade _facade;
        
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Console.WriteLine("OneTimeSetUp");
            _facade = new GameFacade();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Console.WriteLine("OneTimeTearDown");
            _facade = null;
        }
        
        [SetUp]
        public void SetUp()
        {
            Console.WriteLine("Setup");
        }

        [TearDown]
        public void TearDown()
        {
            Console.WriteLine("TearDown");
            
        }    
        
        [Test]
        public void Test_GetAvailableHero()
        {
            const int HERO_ID = 1;
            const string HERO_NAME = "SuperHero";
            var hero = _facade.Player.GetAvailableHero();
            Assert.IsNull(hero);
            _facade.Initializer.AddHero(new Hero(HERO_ID, HERO_NAME));
            hero = _facade.Player.GetAvailableHero();
            Assert.IsNotNull(hero);
            Assert.AreEqual(hero.Name, HERO_NAME);
        }
    }
}